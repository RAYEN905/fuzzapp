//
//  PostModel.h
//  FuzzNews
//
//  Created by Rayen on 3/30/17.
//  Copyright © 2017 GeeksDoByte. All rights reserved.
//

#import "BaseModel.h"



@protocol PostModel<NSObject>
@end
@interface PostModel : BaseModel
@property(strong,nonatomic) NSString<Optional> *id;
@property(strong,nonatomic) NSString<Optional> *type;
@property(strong,nonatomic) NSString<Optional> *date;
@property(strong,nonatomic) NSString<Optional> *data;

@end

//
//  ImageCell.h
//  FuzzNews
//
//  Created by Rayen on 3/30/17.
//  Copyright © 2017 GeeksDoByte. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostModel.h"

@interface ImageCell : UITableViewCell
@property (strong, nonatomic) PostModel * dataModel;
@property (strong, nonatomic) IBOutlet UIImageView *previewImage;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) UIViewController * myviewController;
@end

//
//  PostModel.m
//  FuzzNews
//
//  Created by Rayen on 3/30/17.
//  Copyright © 2017 GeeksDoByte. All rights reserved.
//

#import "PostModel.h"

@implementation PostModel
+(JSONKeyMapper*)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"id": @"id",
                                                                  @"type": @"type",
                                                                  @"date":@"date",
                                                                  @"data":@"data"
                                                                  }];
    
}





@end
            

//
//  ViewController.m
//  FuzzNews
//
//  Created by Rayen on 3/30/17.
//  Copyright © 2017 GeeksDoByte. All rights reserved.
//

#import "ViewController.h"
#import "SVProgressHUD.h"
#import "ApiConstants.h"
#import "ImageCell.h"
#import "TextCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.postDataArr = [[NSMutableArray alloc]init];
    self.imageDataArr = [[NSMutableArray alloc]init];
    self.textDataArr = [[NSMutableArray alloc]init];
    self.dataLoaded = false;
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.tableView.estimatedRowHeight = 50;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    // Do any additional setup after loading the view, typically from a nib.
}



-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [SVProgressHUD show];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self fetchJsonData];
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma tableview deledages

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.dataLoaded){
        return self.rowCount;
    }
    else {
        return 0;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PostModel * cellData = [[PostModel alloc]init];
    switch (self.segFilter.selectedSegmentIndex)
    {
        case 0:{
            NSLog(@"All Filter");
            cellData = [self.postDataArr objectAtIndex:indexPath.row];
            if ([cellData.type  isEqual: @"image"]) {
                ImageCell *imgCell = [self.tableView dequeueReusableCellWithIdentifier:@"imageCell"];
                if (!imgCell){
                    imgCell = [[ImageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"imageCell"];
                }
                imgCell.dataModel = [[PostModel alloc]init];
                imgCell.dataModel = cellData;
                imgCell.dateLabel.text = [self NullFuzz:cellData.date];
                [imgCell.previewImage sd_setImageWithURL:[NSURL URLWithString:cellData.data]
                             placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
                 NSLog(@"Image Filter");
                imgCell.myviewController = self;
                return imgCell;
            }
            if ([cellData.type  isEqual: @"text"]) {
                TextCell *txtCell = [self.tableView dequeueReusableCellWithIdentifier:@"textCell"];
                if (!txtCell){
                    txtCell = [[TextCell  alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"textCell"];
                }
                txtCell.dataModel = [[PostModel alloc]init];
                txtCell.dataModel = cellData;
                txtCell.dateLabel.text = cellData.date;
                txtCell.dataTextView.text = cellData.data;
                NSLog(@"Text Filter");
                txtCell.myviewController = self;
                return txtCell;
            }
        }
            break;
        case 1:{
            cellData = nil;
            cellData = [self.imageDataArr objectAtIndex:indexPath.row];
            NSLog(@"Image Filter");
            ImageCell *imgCell = [self.tableView dequeueReusableCellWithIdentifier:@"imageCell"];
            if (!imgCell){
                imgCell = [[ImageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"imageCell"];
            }
            imgCell.dataModel = [[PostModel alloc]init];
            imgCell.dataModel = cellData;
            imgCell.dateLabel.text = cellData.date;
            [imgCell.previewImage sd_setImageWithURL:[NSURL URLWithString:cellData.data]
                                    placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
            imgCell.myviewController = self;
            return imgCell;
        }
            break;
        case 2:{
            cellData = nil;
            cellData = [self.textDataArr objectAtIndex:indexPath.row];
            NSLog(@"Text Filter");
            TextCell *txtCell = [self.tableView dequeueReusableCellWithIdentifier:@"textCell"];
            if (!txtCell){
                txtCell = [[TextCell  alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"textCell"];
            }
            txtCell.dataModel = [[PostModel alloc]init];
            txtCell.dataModel = cellData;
            txtCell.dateLabel.text = cellData.date;
            txtCell.dataTextView.text = cellData.data;
            
            txtCell.myviewController = self;
            return txtCell;
        }
            break;
        default:
            break;
    }
    return nil;
}







-(void)fetchJsonData{
    NSURL *url = [NSURL URLWithString:API_JSONURL];
    
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url
                                                               completionHandler:^(NSData *data,
                                                                                   NSURLResponse *response,
                                                                                   NSError *error)
                                  {
                                      if (!error)
                                      {
                                          NSError *JSONError = nil;
                                          
                                          NSMutableArray *dictionaryArr = [NSJSONSerialization JSONObjectWithData:data
                                                                                                     options:0
                                                                                                       error:&JSONError];
                                          if (JSONError)
                                          {
                                              NSLog(@"Serialization error: %@", JSONError.localizedDescription);
                                          }
                                          else
                                          {
                                              NSLog(@"Response: %@", dictionaryArr);
                                              
                                              NSError *ModelError = nil;
                                              for(NSDictionary *dataDict in dictionaryArr){
                                                  PostModel * dataModel = [[PostModel alloc]initWithDictionary:dataDict error:&ModelError];
                                                  if (!ModelError){
                                                      if([dataModel.type  isEqual: @"text"] || [dataModel.type  isEqual: @"image"]){
                                                          if (dataModel.date.length >0 && dataModel.data.length > 0){
                                                      [self.postDataArr addObject:dataModel];
                                                      NSLog(@"Model Array: %@", self.postDataArr);
                                                      self.dataLoaded = true;
                                                          }
                                                      }
                                                  }
                                                  else
                                                  {
                                                      NSLog(@"Error: %@", ModelError.localizedDescription);
                                                  }
                                                  
                                                  
                                                  
                                              }
                                              [self filterMagic];
                                          }
                                      }
                                      else
                                      {
                                          NSLog(@"Error: %@", error.localizedDescription);
                                      }
                                  }];
    [task resume];
}


- (NSString*)NullFuzz:(NSString*)str{
    
    if(str.length > 0){
        return str;
    }else {
        return @"";
    }
    
    
}


- (void) filterMagic {
    switch (self.segFilter.selectedSegmentIndex) {
        case 0:
            self.rowCount = (int)self.postDataArr.count;
            [self reloadTableMT];
            break;
        case 1:
            [self.imageDataArr removeAllObjects];
            for (PostModel *obj in self.postDataArr) {
                if ([obj.type  isEqual: @"image"] && obj.data.length > 0){
                    [self.imageDataArr addObject:obj];
                }
                

            }
            NSLog(@"%@", self.imageDataArr);
            self.rowCount = (int)self.imageDataArr.count;
            [self reloadTableMT];
            break;
        case 2:
            [self.textDataArr removeAllObjects];
            for (PostModel *obj in self.postDataArr) {
                if ([obj.type  isEqual: @"text"] && obj.data.length > 0){
                    [self.textDataArr addObject:obj];
                }
                

            }
            NSLog(@"%@", self.textDataArr);
            self.rowCount = (int)self.textDataArr.count;
            [self reloadTableMT];
            break;
                
                
        default:
        break;
    }
    
    
}


- (void) reloadTableMT{
    [self.tableView performSelectorOnMainThread:@selector(reloadData)
                                     withObject:nil
                                  waitUntilDone:NO];
}






- (IBAction)selectFilter:(UISegmentedControl *)sender {
    switch (self.segFilter.selectedSegmentIndex)
    {
        case 0:
            NSLog(@"All Filter");
            [self filterMagic];
            break;
        case 1:
            NSLog(@"Image Filter");
             [self filterMagic];
            break;
        case 2:
            NSLog(@"Text Filter");
             [self filterMagic];
            break;
        default:
            break; 
    }
}





@end

//
//  ApiConstants.m
//  FuzzNews
//
//  Created by Rayen on 3/30/17.
//  Copyright © 2017 GeeksDoByte. All rights reserved.
//

#import "ApiConstants.h"

@implementation ApiConstants
NSString * const API_JSONURL = @"http://quizzes.fuzzstaging.com/quizzes/mobile/1/data.json";
NSString * const WEB_URL = @"https://fuzzproductions.com/";
@end

//
//  FullImageVC.m
//  FuzzNews
//
//  Created by Rayen on 3/31/17.
//  Copyright © 2017 GeeksDoByte. All rights reserved.
//

#import "FullImageVC.h"

@interface FullImageVC ()

@end

@implementation FullImageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.fullImage sd_setImageWithURL:[NSURL URLWithString:self.dataModel.data]
                    placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

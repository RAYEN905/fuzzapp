//
//  TextCell.h
//  FuzzNews
//
//  Created by Rayen on 3/30/17.
//  Copyright © 2017 GeeksDoByte. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostModel.h"

@interface TextCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UITextView *dataTextView;
@property (strong, nonatomic) PostModel * dataModel;
@property (strong, nonatomic) UIViewController * myviewController;
@end

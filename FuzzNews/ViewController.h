//
//  ViewController.h
//  FuzzNews
//
//  Created by Rayen on 3/30/17.
//  Copyright © 2017 GeeksDoByte. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostModel.h"

@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UISegmentedControl *segFilter;
typedef enum {All = 0,Image  = 1,Text = 2,}fuzzFilters;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray<PostModel*> * postDataArr;
@property (strong, nonatomic) NSMutableArray<PostModel*> * imageDataArr;
@property (strong, nonatomic) NSMutableArray<PostModel*> * textDataArr;
@property (strong, nonatomic) PostModel * dataModel;
@property (nonatomic, assign) int rowCount;
@property (nonatomic, assign) BOOL dataLoaded;
@end


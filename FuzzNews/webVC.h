//
//  webVC.h
//  FuzzNews
//
//  Created by Rayen on 3/31/17.
//  Copyright © 2017 GeeksDoByte. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostModel.h"
#import "ApiConstants.h"

@interface webVC : UIViewController
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) PostModel * dataModel;
@end

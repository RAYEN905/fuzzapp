//
//  BaseModel.h
//  FuzzNews
//
//  Created by Rayen on 3/30/17.
//  Copyright © 2017 GeeksDoByte. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@protocol BaseModel <NSObject>

@end

@interface BaseModel : JSONModel

@end

//
//  TextCell.m
//  FuzzNews
//
//  Created by Rayen on 3/30/17.
//  Copyright © 2017 GeeksDoByte. All rights reserved.
//

#import "TextCell.h"
#import "webVC.h"

@implementation TextCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [singleTap setNumberOfTapsRequired:1];
    
    [self.dataTextView addGestureRecognizer:singleTap];
}


-(void)handleSingleTap:(id)sender {
    
    UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main"
                                                  bundle:nil];
    webVC* vc = [sb instantiateViewControllerWithIdentifier:@"webVC"];
    vc.dataModel = self.dataModel;
    [self.myviewController.navigationController pushViewController:vc animated:YES];
    
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  AppDelegate.h
//  FuzzNews
//
//  Created by Rayen on 3/30/17.
//  Copyright © 2017 GeeksDoByte. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


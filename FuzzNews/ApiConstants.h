//
//  ApiConstants.h
//  FuzzNews
//
//  Created by Rayen on 3/30/17.
//  Copyright © 2017 GeeksDoByte. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ApiConstants : NSObject
extern NSString * const API_JSONURL;
extern NSString * const WEB_URL;
@end

//
//  ImageCell.m
//  FuzzNews
//
//  Created by Rayen on 3/30/17.
//  Copyright © 2017 GeeksDoByte. All rights reserved.
//

#import "ImageCell.h"
#import "FullImageVC.h"
#import <SDWebImage/UIImageView+WebCache.h>
@implementation ImageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [singleTap setNumberOfTapsRequired:1];
    
    [self.previewImage addGestureRecognizer:singleTap];
}


-(void)handleSingleTap:(id)sender {
    
    UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main"
                                                  bundle:nil];
    FullImageVC* vc = [sb instantiateViewControllerWithIdentifier:@"fullImageVC"];
    vc.dataModel = self.dataModel;
    [self.myviewController.navigationController pushViewController:vc animated:YES];

    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  FullImageVC.h
//  FuzzNews
//
//  Created by Rayen on 3/31/17.
//  Copyright © 2017 GeeksDoByte. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostModel.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface FullImageVC : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *fullImage;
@property (strong, nonatomic) PostModel * dataModel;
@end
